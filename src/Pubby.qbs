// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

QtApplication {
    name: "org.kde.Pubby"

    install: true
    installDir: "bin"

    files: [
        "*.cpp",
        "*.h",
    ]

    Group {
        files: ["../data/**"]
        fileTags: "qt.core.resource_data"
        Qt.core.resourceSourceBase: "../data/"
        Qt.core.resourcePrefix: "/"
    }
    Group {
        files: ["../node_modules/epubjs/dist/epub.js"]
        fileTags: "qt.core.resource_data"
        Qt.core.resourceSourceBase: "../node_modules/epubjs/dist/"
        Qt.core.resourcePrefix: "/web"
    }

    Depends { name: "Qt"; submodules: ["widgets", "qml", "quick", "webengine"] }
}
