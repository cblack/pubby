// SPDX-FileCopyrightText: 2021 Carson Black <uhhadd@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import org.kde.kirigami 2.15 as Kirigami
import QtQuick.Window 2.10
import QtWebEngine 1.10

Window {
    visible: true

    WebEngineView {
        width: parent.width
        height: parent.height
        url: "web/main.html"
    }
}
